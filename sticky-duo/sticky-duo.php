<?php
/**
 * Sticky Duo By Todd Durica.
 *
 * Plugin Name:		Sticky Duo
 * Plugin URI:		http://td1.me
 * Description:		Configure sticky header and sticky footer within any wordpress theme.
 * Author:			Todd Durica
 * Author URI:		http://td1.me
 */


// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

require_once( plugin_dir_path( __FILE__ ) . 'sticky-duo-class.php' );
require_once( plugin_dir_path( __FILE__ ) . 'sticky-duo-settings.php' );

add_action( 'plugins_loaded', array( 'Sticky_Duo', 'tdsdfn_get_instance' ) );