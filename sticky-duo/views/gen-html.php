<?php
/**
* Represents the view for the public-facing component of the plugin.
*
* This typically includes any information, if any, that is rendered to the
* frontend of the theme when the plugin is activated.
 *
 * it's currently controlling:
 * 1. wether a logo image or blogname appears in $tdsd_widelogo
 * 2. wether a menu appears or not
*/
// Get Sticky Duo options
$tdsd_pub_settings = get_option( 'tdsd' );
// Check if there is a logo image
// esc_attr() - Filter a string cleaned and escaped for output in an HTML attribute stripped of invalid or special characters before output
if ( '' != $tdsd_pub_settings['wide_state_logo'] ) :
	$tdsd_widelogo = '<img src="' . esc_attr( $tdsd_pub_settings['wide_state_logo'] ) . '" alt="' . esc_attr( get_bloginfo( 'description' ) ) . '" />';
else :
	$tdsd_widelogo = get_bloginfo( 'name' );
endif;
if ( '' != $tdsd_pub_settings['narrow_state_expandicon'] ) :
	$tdsd_expandicon = '<img src="' . esc_attr( $tdsd_pub_settings['narrow_state_expandicon'] ) . '" />';
else :
	$tdsd_expandicon = get_bloginfo( 'name' );
endif;
?>
<style>

</style>

<div id="stickyduo">
	<div id="stickyduo-inner">
		<div id="stickyduo-logo">
			<a href="<?php echo bloginfo( 'url' ); ?>" title="<?php bloginfo( 'description' ); ?>">
				<?php echo $tdsd_widelogo; ?> <!--display logo image wrapped in a detailed a element-->
			</a>
		</div>
		<!--(+) IF greater than threshold  -->

		<!--(+) AND IF 'menu' isset  -->
		<?php if( isset($tdsd_pub_settings['menu']) ) :
			$menu_args = array(
				'menu'			=> $tdsd_pub_settings['menu'],
				'depth'			=> 1,
				'menu_id'		=> 'stickyduo-menu',
				'container'		=> '',
				'fallback_cb'	=> ''
			);
			wp_nav_menu( $menu_args );
		endif; ?>
		<div id="stickyduo-mobile-toggle">
		<?php if ( '' != $tdsd_pub_settings['narrow_state_expandicon'] ) : ?>
			<!--display the image, doesnt need 'a' element-->
			<?php echo $tdsd_expandicon; ?>
		<?php else : ?>

			<span></span>
			<span></span>
			<span></span>
		<?php endif; ?>

		</div>
	</div><!-- #stickyduo-inner -->
</div><!-- #stickyduo -->

<!-- IF span expandicon is selected, #stickyduo-mobile-toggle span{display: block; }
 	IF an image is chosen, set #stickyduo-mobile-toggle span{display: none; }
 		-and insert the image into #stickyduo-mobile-toggle


 		Give options to control:
 		-Logo size and position
 		-
 		-
 		-
 		-
 	-->