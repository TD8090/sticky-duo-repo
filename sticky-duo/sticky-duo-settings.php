<?php
/**
 * Registers New Customizer Options.
 */





add_action( 'customize_register', 'tdsdfn_customize_register' );

/** * Registers all Customizer options. */
function tdsdfn_customize_register( $wp_customize ) {
	$author_plugin_slug = 'td-stickyduo';

	// Define Number custom control
	if ( class_exists( 'WP_Customize_Control') ) :
		class Sticky_Duo_Number_Control extends WP_Customize_Control {
			public $type = 'number';
			
			public function render_content() { ?>
				<label>
					<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
					<input class="small-text" type="number" value="<?php echo esc_attr( $this->value() ); ?>" <?php $this->link(); ?> />
				</label>
			<?php }
		}
	endif;




	$wp_customize->add_section(
		'tdsd',
		array(
			'title'			=> __( 'Sticky Duo Settings', $author_plugin_slug ),
			'priority'		=> 1
		) 
	);


	// [show_at] Sticky Header show at
	$wp_customize->add_setting(
		'tdsd[show_at]',
		array(
			'default'			=> '100',
			'sanitize_callback' => 'wp_filter_nohtml_kses',
			'type'				=> 'option',
			'capability'		=> 'edit_theme_options',
		)
	);
	$wp_customize->add_control(
		new Sticky_Duo_Number_Control(
			$wp_customize,
			'tdsd[show_at]',
			array(
				'label'		=> __( 'Show at Scroll distance (px dist)', $author_plugin_slug ),
				'section'	=> 'tdsd',
				'settings'	=> 'tdsd[show_at]',
				'priority'	=> 5
			)
		)
	);

	// [wn_threshold] Sticky Header Wide-Narrow Threshold
	$wp_customize->add_setting(
		'tdsd[wn_threshold]',
		array(
			'default'			=> '600',
			'sanitize_callback' => 'wp_filter_nohtml_kses',
			'type'				=> 'option',
			'capability'		=> 'edit_theme_options',
		)
	);
	$wp_customize->add_control(
		new Sticky_Duo_Number_Control(
			$wp_customize,
			'tdsd[wn_threshold]',
			array(
				'label'		=> __( 'Wide-Narrow Threshold (px width)', $author_plugin_slug ),
				'section'	=> 'tdsd',
				'settings'	=> 'tdsd[wn_threshold]',
				'priority'	=> 10
			)
		)
	);

	// [menu] Sticky Header menu
	$menus = wp_get_nav_menus();
	if ( $menus ) :
		$choices = array( 0 => __( '&mdash; Select a menu &mdash;' ) );
		foreach ( $menus as $menu ) :
			$choices[ $menu->term_id ] = wp_html_excerpt( $menu->name, 40, '&hellip;' );
		endforeach;

		$wp_customize->add_setting(
			'tdsd[menu]',
			array(
				'sanitize_callback' => 'absint',
				'theme_supports'    => 'menus',
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
			)
		);
		$wp_customize->add_control(
			'tdsd[menu]',
				array(
				'label'   	=> __( 'Menu Select', $author_plugin_slug ),
				'section' 	=> 'tdsd',
				'type'    	=> 'select',
				'choices' 	=> $choices,
				'priority'	=> 15
			)
		);
	endif;

	// [wide_state_logo] Upload Sticky Header logo for wide-state
	$wp_customize->add_setting(
		'tdsd[wide_state_logo]',
		array('default'	=> '',		'sanitize_callback' => 'esc_url_raw',
			  'type'	=> 'option','capability'=> 'edit_theme_options',		)	);
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'tdsd[wide_state_logo]',
			array(
				'label'		=> __( 'Wide-State Logo Image', $author_plugin_slug ),
				'section'	=> 'tdsd',
				'settings'	=> 'tdsd[wide_state_logo]',
				'priority'	=> 20
	)));
	// [wide_state_logo] Upload Sticky Header logo for wide-state
	$wp_customize->add_setting(
		'tdsd[background_image]',
		array('default'	=> '',		'sanitize_callback' => 'esc_url_raw',
			  'type'	=> 'option','capability'=> 'edit_theme_options',		)	);
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'tdsd[background_image]',
			array(
				'label'		=> __( 'Background Image (repeat x)', $author_plugin_slug ),
				'section'	=> 'tdsd',
				'settings'	=> 'tdsd[background_image]',
				'priority'	=> 21
	)));

	// [narrow_state_logo] Header Narrow-state logo image
/*	$wp_customize->add_setting(
		'tdsd[narrow_state_logo]',
		array(
			'default'			=> '',
			'sanitize_callback' => 'esc_url_raw',
			'type'				=> 'option',
			'capability'		=> 'edit_theme_options',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'tdsd[narrow_state_logo]',
			array(
				'label'		=> __( 'Narrow-State Logo Image', $author_plugin_slug ),
				'section'	=> 'tdsd',
				'settings'	=> 'tdsd[narrow_state_logo]',
				'priority'	=> 25,

			)
		)
	);*/
	// [narrow_state_expandicon] Header Narrow-state expand icon
	$wp_customize->add_setting(
		'tdsd[narrow_state_expandicon]',
		array(
			'default'			=> '',
			'sanitize_callback' => 'esc_url_raw',
			'type'				=> 'option',
			'capability'		=> 'edit_theme_options',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'tdsd[narrow_state_expandicon]',
			array(
				'label'		=> __( 'Narrow-State Expand-Icon (>25px)', $author_plugin_slug ),
				'section'	=> 'tdsd',
				'settings'	=> 'tdsd[narrow_state_expandicon]',
				'priority'	=> 30

			)
		)
	);

	//NEEDS WORK
	// [wide_state_height] Sticky Header Wide-state height
	$wp_customize->add_setting(
		'tdsd[wide_state_height]',
		array(	'default' => '30',		'sanitize_callback' => 'esc_url_raw',
			'type'	  => 'option',  'capability'		=> 'edit_theme_options',	));
	$wp_customize->add_control(
		new Sticky_Duo_Number_Control(
			$wp_customize,
			'tdsd[wide_state_height]',
			array(
				'label'		=> __( 'Wide-State height (px)', $author_plugin_slug ),
				'section'	=> 'tdsd',
				'settings'	=> 'tdsd[wide_state_height]',
				'priority'	=> 35
	)));

	// [narrow_state_height] Sticky Header Narrow-state height
	$wp_customize->add_setting(
		'tdsd[narrow_state_height]',
		array(	'default' => '20',		'sanitize_callback' => 'esc_url_raw',
				'type'	  => 'option',  'capability'		=> 'edit_theme_options',	));
	$wp_customize->add_control(
		new Sticky_Duo_Number_Control(
			$wp_customize,
			'tdsd[narrow_state_height]',
			array(
				'label'		=> __( 'Narrow-State height (px)', $author_plugin_slug ),
				'section'	=> 'tdsd',
				'settings'	=> 'tdsd[narrow_state_height]',
				'priority'	=> 40
	)));







//EVERYTHINGS OK HERE
	// [background_color] Sticky Header background color
	$wp_customize->add_setting(
		'tdsd[background_color]',
		array(
			'default'			=> '#181818',
			'sanitize_callback' => 'wp_filter_nohtml_kses', // Used instead of HTMLPurifier
			'type'				=> 'option',
			'capability'		=> 'edit_theme_options',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'tdsd[background_color]',
			array(
				'label'		=> __( 'Background color', $author_plugin_slug ),
				'section'	=> 'tdsd',
				'settings'	=> 'tdsd[background_color]',
				'priority'	=> 45
			)
		)
	);
//EVERYTHINGS OK HERE
	// [text_color] Sticky Header text color
	$wp_customize->add_setting(
		'tdsd[text_color]',
		array(
			'default'			=> '#f9f9f9',
			'sanitize_callback' => 'wp_filter_nohtml_kses',
			'type'				=> 'option',
			'capability'		=> 'edit_theme_options',
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'tdsd[text_color]',
			array(
				'label'		=> __( 'Text color', $author_plugin_slug ),
				'section'	=> 'tdsd',
				'settings'	=> 'tdsd[text_color]',
				'priority'	=> 50
			)
		)
	);
	//TODO NARROW LOGO POSITION (LEFT OR CENTER) hmmmm....later
	// [inner_max_width] Sticky Header Logo/Menu max width
	$wp_customize->add_setting(
		'tdsd[inner_max_width]',
		array(	'default' => '1200',  'sanitize_callback' => 'wp_filter_nohtml_kses',
			'type'	  => 'option','capability'		  => 'edit_theme_options',		));
	$wp_customize->add_control(
		new Sticky_Duo_Number_Control(
			$wp_customize,
			'tdsd[inner_max_width]',
			array(
				'label'	    => __( 'Logo/Menu container max width, widest needed (px width) (Wide or unset setting pins apart left & right. Narrow setting forces stack effect)', $author_plugin_slug ),
				'section'	=> 'tdsd',
				'settings'	=> 'tdsd[inner_max_width]',
				'priority'	=> 55
			)
		)
	);
	$wp_customize->add_setting(
		'tdsd[inner_medium_mxw_bpoint]',
		array(	'default' => '1200',	  'sanitize_callback' => 'wp_filter_nohtml_kses',
				'type'	  => 'option','capability'		  => 'edit_theme_options',		));
	$wp_customize->add_control(
		new Sticky_Duo_Number_Control(
			$wp_customize,
			'tdsd[inner_medium_mxw_bpoint]',
			array(
				'label'	    => __( 'UNTIL medium trigger at (px width breakpoint)(near above value)', $author_plugin_slug ),
				'section'	=> 'tdsd',
				'settings'	=> 'tdsd[inner_medium_mxw_bpoint]',
				'priority'	=> 56
			)
		)
	);
	$wp_customize->add_setting(
		'tdsd[inner_medium_mxw]',
		array(	'default' => '980',	  'sanitize_callback' => 'wp_filter_nohtml_kses',
				'type'	  => 'option','capability'		  => 'edit_theme_options',		));
	$wp_customize->add_control(
		new Sticky_Duo_Number_Control(
			$wp_customize,
			'tdsd[inner_medium_mxw]',
			array(
				'label'	    => __( 'Where (px max width) is needed...', $author_plugin_slug ),
				'section'	=> 'tdsd',
				'settings'	=> 'tdsd[inner_medium_mxw]',
				'priority'	=> 57
			)
		)
	);

		$wp_customize->add_setting(
		'tdsd[inner_narrow_mxw_bpoint]',
		array(	'default' => '980',	  'sanitize_callback' => 'wp_filter_nohtml_kses',
				'type'	  => 'option','capability'		  => 'edit_theme_options',		));
	$wp_customize->add_control(
		new Sticky_Duo_Number_Control(
			$wp_customize,
			'tdsd[inner_narrow_mxw_bpoint]',
			array(
				'label'	    => __( 'UNTIL narrow trigger at (px width breakpoint)(near above value)', $author_plugin_slug ),
				'section'	=> 'tdsd',
				'settings'	=> 'tdsd[inner_narrow_mxw_bpoint]',
				'priority'	=> 58
			)
		)
	);
	$wp_customize->add_setting(
		'tdsd[inner_narrow_mxw]',
		array(	'default' => '770',	  'sanitize_callback' => 'wp_filter_nohtml_kses',
				'type'	  => 'option','capability'		  => 'edit_theme_options',		));
	$wp_customize->add_control(
		new Sticky_Duo_Number_Control(
			$wp_customize,
			'tdsd[inner_narrow_mxw]',
			array(
				'label'	    => __( 'Where (px max width) is needed...', $author_plugin_slug ),
				'section'	=> 'tdsd',
				'settings'	=> 'tdsd[inner_narrow_mxw]',
				'priority'	=> 59
			)
		)
	);

	
	
	
	
	
	
	
	
	
	
	//narrow settings
	// Sticky Header change to smaller logo graphic + change menu to collapsed at specific width.

//EVERYTHINGS OK HERE
	// [disable_if_narrower] Sticky Header DISABLE if narrower than
	$wp_customize->add_setting(
		'tdsd[disable_if_narrower]',
		array(
			'default'			=> '10',
			'sanitize_callback' => 'wp_filter_nohtml_kses',
			'type'				=> 'option',
			'capability'		=> 'edit_theme_options',
		)
	);
	$wp_customize->add_control(
		new Sticky_Duo_Number_Control(
			$wp_customize,
			'tdsd[disable_if_narrower]',
			array(
				'label'		=> __( 'DISABLE if narrower (px width)', $author_plugin_slug ),
				'section'	=> 'tdsd',
				'settings'	=> 'tdsd[disable_if_narrower]',
				'priority'	=> 60
			)
		)
	);



}  // END: function tdsdfn_customize_register( $wp_customize )



/**
 * Returns plugin settings.
 *
 * @return    array    Merged array of plugin settings and plugin defaults.
 */
function tdsdfn_get_settings() {
	$plugin_defaults = array(
		'background_color'		=> '#d2d2d2',
		'text_color'			=> '#121212',
		'show_at'				=> '100',
		'wn_threshold'			=> '600',
		'inner_max_width'			=> '1200',
		'inner_medium_mxw_bpoint'	=> '1200',
		'inner_medium_mxw'			=> '980',
		'inner_narrow_mxw_bpoint'	=> '980',
		'inner_narrow_mxw'			=> '770',
		'disable_if_narrower'	=> '10',

	);
	$plugin_settings = get_option( 'tdsd' );

	return wp_parse_args( $plugin_settings, $plugin_defaults ); // Merge user defined arguments into defaults array
}