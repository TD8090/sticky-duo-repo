(function ($) {
	"use strict";
	$(document).ready(function () {
		tdsd();

		/*DURICA*/
		var $elstickyduo = $('#stickyduo');
		var $elmenu = $('#stickyduo-menu');
		var $eltoggle = $('#stickyduo-mobile-toggle');
		// Mobile Navigation
		//expandicon  TODO::CONVERT TO SCROLL-TOP BUTTON
		$eltoggle.click(function() {
			if ($elstickyduo.hasClass('open-nav')) {
				$elstickyduo.removeClass('open-nav');
			} else {
				$elstickyduo.addClass('open-nav');
			}
		});
		//close the menu if a choice is made
		$elmenu.find("a").click(function() {
			if ($elstickyduo.hasClass('open-nav')) {
				$elstickyduo.removeClass('open-nav');
			}
		});

		//listen for all clicks everywhere to close the nav menu
		$('html').click(function() {
			$elstickyduo.removeClass('open-nav');
		});
		//but if a click is heard on stickyduo or its children, dont let the event through
		$('#stickyduo').click(function(event){
			event.stopPropagation();
		});
		//END||tdsd function
	});

	$(window).resize(function () {
		tdsd();
	});

	$(window).scroll(function () {
		tdsd();
	});
	<!--(+) IF greater than threshold  -->

	function tdsd() {
		// Check browser window width and pass the disable-if-narrower check
		if ($(window).scrollTop() > StickyDuoParams.show_at && $(window).width() > StickyDuoParams.disable_if_narrower) {
			$('#stickyduo').addClass('is-sticky');			// Show
		} else {
			$('#stickyduo').removeClass('is-sticky');		// Hide
		}

		/*
		if ($(window).scrollTop() > StickyDuoParams.show_at && $(window).width() > StickyDuoParams.disable_if_narrower) {
			$('#stickyduo').stop().animate({"top": '0'}, 500);			// Show
		} else {
			$('#stickyduo').stop().animate({"top": '-200'}, 500);		// Hide
		}
*/
	}

	//future ref: StickyDuoParams.wn_threshold


}(jQuery));


